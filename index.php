<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
    <title>Realtime Italia</title>
    <script
            src="assets/js/jq.js"></script>


    <link href="https://fonts.googleapis.com/css?family=Overpass:200,400,900" rel="stylesheet">
    <link href="assets/style.css" rel="stylesheet">
    <link href="assets/transition.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <script src="assets/js/main.js" type="text/javascript"></script>
</head>
<body>
<div class="cookie_message">
<p>Per migliorare il nostro sito, la tua esperienza di navigazione e la fruizione pubblicitaria questo sito web utilizza i cookies (proprietari e di terze parti). Se continui la navigazione accetti di utilizzarli. Per maggiori informazioni (ad esempio su come disabilitarli) leggi la nostra<a href="cookie.html" target="_blank"> Cookie Policy</a> <a href="#" id="accetta"> ACCETTO</a></p>
</div>
<div class="bg central_container">
    <div class="animated_line animated"></div>
    <main class="cd-main-content">

        <div class="square">
            <div class="circle">
                <img src="assets/img/logo.png"/>
            </div>
        </div>

    </main>

    <div class="cd-modal">
        <div class="modal-content">

            <div class="back_to_realtime">
                <a href="http://www.realtimetv.it">< Torna a <span>realtimetv.it</span></a>
            </div>

            <div class="text_wrapper">
                <h1 class="realtime_text">
                    <span> Ciao!</span> <br/>
                    qualcosa di <br/>
                    fantastico <br/>
                    sta per<br/>
                    succedere <br/> <span>qui</span>
                </h1>
                <div class="scroll_down_mobile"></div>
            </div>

            <div class="cart_wrapper">



            <?php
            date_default_timezone_set('Europe/Rome');


            $start = new DateTime('2017-09-1 12:00:00.000000');
            $end = new DateTime('2017-09-1 20:00:00.000000');
            $now = new DateTime();

            if( $now > $start && $now < $end) {
            ?>
             <div class="card left_card box">
                                           <a href="http://it.dplay.com/realtime/bake-off-italia/stagione-5-episodio-1/" class="link">


                                               <div class="img_wrapper">
                                                   <div class="triangle-down-right"></div>
                                                   <img src="assets/img/card1.png" class="image"/>

                                               </div>
                                               <p>Guarda online in esclusiva la prima puntata</p></a>
                                       </div>



                <div class="card right_card box">
                    <a href="http://it.dplay.com" class="link">
                        <div class="img_wrapper">
                            <div class="triangle-down-right"></div>
                            <img src="assets/img/card2ok.png" class="image"/>

                        </div>
                        <p>I tuoi programmi preferiti sono su Dplay</p></a>
                </div>

            <?php
            }
            else{
           ?>
            <div class="card left_card box">
                               <a href="http://bakeoffitalia.realtime.it/" class="link">


                                   <div class="img_wrapper">
                                       <div class="triangle-down-right"></div>
                                       <img src="assets/img/card1.png" class="image"/>

                                   </div>
                                   <p>Entra nel dolcissimo mondo di Bake Off Italia</p></a>
                           </div>



                <div class="card right_card box">
                    <a href="http://it.dplay.com" class="link">
                        <div class="img_wrapper">
                            <div class="triangle-down-right"></div>
                            <img src="assets/img/card2ok.png" class="image"/>

                        </div>
                        <p>Guarda i tuoi programmi preferiti su Dplay</p></a>
                </div>


           <?php
            }
            ?>








            </div>
<div class="clear"></div>

            <div class="social_newsletter_wrapper box">

                <div>
                    <h1>Resta sintonizzato!</h1>
                    <div class="mail_register">


                        <!-- Begin MailChimp Signup Form -->
                        <link href="//cdn-images.mailchimp.com/embedcode/horizontal-slim-10_7.css" rel="stylesheet" type="text/css">

                        <div id="mc_embed_signup">
                            <form action="//realtime.us16.list-manage.com/subscribe/post?u=bd8165298d12419f7a1370ef3&amp;id=8f4ae1cf1b" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
                                <div id="mc_embed_signup_scroll">

                                    <input type="email" value="" name="EMAIL" class="email" id="mce-EMAIL" placeholder="Inserisci il tuo indirizzo e-mail" required>
                                    <p class="mail_err"> > Attenzione, inserire un indirizzo e-mail valido</p>
                                    <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
                                    <div style="position: absolute; left: -5000px;" aria-hidden="true">
                                        <input type="text" name="b_bd8165298d12419f7a1370ef3_8f4ae1cf1b" tabindex="-1" value=""></div>
                                    <div class="clear"><input type="submit" value="" name="subscribe" id="mc-embedded-subscribe" class="button"></div>

                                    <label id="checkbox_mandatory">
                                        Letta e compresa la <a href="privacy.html" target="_blank">Privacy Policy</a>, espressamente e liberamente
                                        il trattamento dei miei dati personali per la finalità di invio della newsletter editoriale attraverso i miei recapiti privati.
<br/>
                                        <input type="checkbox" name="mc4wp-subscribe" id="acceptterms" class="checkb" value="1" />
                                       <span class="consento">CONSENTO</span>
                                        <input type="checkbox" name="mc4wp-subscribe" id="denyterms" class="checkb" value="1" />
                                     <span> NON CONSENTO</span>
                                    </label>
                                </div>
                            </form>
                        </div>

                        <!--End mc_embed_signup-->

                    </div>
                </div>
                <div class="clear"></div>
            </div>


            <div class="scroll_down"></div>


        </div>


        <div class="social_wrap">
            <a href="https://www.facebook.com/realtimeitalia/" class="fa fa-facebook animated" target="_blank"></a>
            <a href="https://twitter.com/realtimetvit" class="fa fa-twitter animated" target="_blank"></a>
            <a href="https://www.instagram.com/realtimetvit/?hl=it" class="fa fa-instagram animated" target="_blank"></a>
        </div>

    </div>


    <div class="cd-transition-layer">
        <div class="bg-layer"></div>
    </div>





</div>
<script>

if(getCookie('accetta_cookie') == ''){

 $('.cookie_message').fadeIn();

 $('#accetta').click(function(e){
    $('.cookie_message').fadeOut();
    setCookie('accetta_cookie', 'ok', 360);

    console.log('cookie set');
    })

}


    function setCookie(cname, cvalue, exdays) {
        var d = new Date();
        d.setTime(d.getTime() + (exdays*24*60*60*1000));
        var expires = "expires="+ d.toUTCString();
        document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
    }

    function getCookie(cname) {
        var name = cname + "=";
        var decodedCookie = decodeURIComponent(document.cookie);
        var ca = decodedCookie.split(';');
        for(var i = 0; i <ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == ' ') {
                c = c.substring(1);
            }
            if (c.indexOf(name) == 0) {
                return c.substring(name.length, c.length);
            }
        }
        return "";
    }

</script>


<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-101118425-1', 'auto');
  ga('send', 'pageview');

</script>

</body>
</html>