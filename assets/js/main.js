jQuery(document).ready(function($){

    var modalTrigger = $('.cd-modal-trigger'),
        transitionLayer = $('.cd-transition-layer'),
        transitionBackground = transitionLayer.children(),
        modalWindow = $('.cd-modal');


    // CHECK ON MCHIMP
    $('#mc-embedded-subscribe').click(function(e) {


        if ($('#acceptterms').prop('checked') == false || $('#denyterms').prop('checked') == true) {
            if(!validateEmail($('.email').val())){
                $('.mail_err').fadeIn()
                $('.consento').css('color','#333')
                setTimeout(function(){

                    $('.mail_err').fadeOut();
                },2500)
                return false;
            }
            $('.consento').css('color','#BD0700')
            return false;
        }
    });

    $('input[type="checkbox"]').on('change', function() {
        $('input[type="checkbox"]').not(this).prop('checked', false);
        $('.consento').css('color','#333')
    });


    function validateEmail(email) {
        var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(email);
    }


    setTimeout(function(){
        $('.square').hide();
    },3200)

    setTimeout(function(){
        transitionLayer.addClass('visible opening');
        var delay = 800;
        setTimeout(function(){

            // INIT CONTENT
            modalWindow.addClass('visible');
            $('.text_wrapper').addClass('text_loaded')
            $('.text_wrapper').css('animation-name', 'animateShadow')
            $('.animated_line').addClass('full')
            $('body, html').css('overflow-y','scroll')



        }, delay);
    },3000)



    var frameProportion = 1.78,
        frames = 25,
        resize = false;

    setLayerDimensions();
    $(window).on('resize', function(){
        if( !resize ) {

            resize = true;
            (!window.requestAnimationFrame) ? setTimeout(setLayerDimensions, 300) : window.requestAnimationFrame(setLayerDimensions);
        }
    });





    function setLayerDimensions() {
        var windowWidth = $(window).width(),
            windowHeight = $(window).height(),
            layerHeight, layerWidth;

        if( windowWidth/windowHeight > frameProportion ) {
            layerWidth = windowWidth;
            layerHeight = layerWidth/frameProportion;
        } else {
            layerHeight = windowHeight*1.2;
            layerWidth = layerHeight*frameProportion;
        }

        transitionBackground.css({
            'width': layerWidth*frames+'px',
            'height': layerHeight+'px',
        });

        resize = false;
    }



});




$(window).scroll(function() {
    if($(window).width() > 700 && window.innerHeight < window.innerWidth){
        var scrollTop = $(this).scrollTop();
        var winheight=$(window).height()
        var converted= convertRange( scrollTop, [ 0, winheight ], [ 1, 600 ] );
        var convertedCard= convertRange( scrollTop, [ 0, winheight ], [ 1, 800 ] );
        var convertedOpacity= convertRange( scrollTop, [ 0, winheight ], [ 1, 0 ] );
        var convertedFontSize= convertRange( scrollTop, [ 0, winheight ], [ 5, 15] );
        var convertSocial = convertRange( scrollTop, [ 0, winheight ], [ -300, 40] );

        $('.back_to_realtime').css(
            {left:-converted+'px'}
        )
        if(scrollTop < winheight-100){
            $('.realtime_text').css({
                left: converted+'px',
                opacity:convertedOpacity+0.1,
                fontSize:convertedFontSize+'vw',
                lineHeight:convertedFontSize+'vw',
            });

        }
        $('.social_wrap').css({left:convertSocial+'px'});

        $('.card').css({
            right: -convertedCard+'px',
        });


        $('.scroll_down').css({
            opacity:convertedOpacity-0.5
        });
    }

});
function convertRange( value, r1, r2 ) {
    return ( value - r1[ 0 ] ) * ( r2[ 1 ] - r2[ 0 ] ) / ( r1[ 1 ] - r1[ 0 ] ) + r2[ 0 ];
}

