jQuery(document).ready(function($){

	var modalTrigger = $('.cd-modal-trigger'),
		transitionLayer = $('.cd-transition-layer'),
		transitionBackground = transitionLayer.children(),
		modalWindow = $('.cd-modal');


    setTimeout(function(){
        $('.square').hide();
    },3200)

setTimeout(function(){
    transitionLayer.addClass('visible opening');
    var delay = 800;
    setTimeout(function(){

        // INIT CONTENT
        modalWindow.addClass('visible');
		$('.text_wrapper').addClass('text_loaded')
        $('.text_wrapper').css('animation-name', 'animateShadow')
        $('.animated_line').addClass('full')
        $('body, html').css('overflow-y','auto')



    }, delay);
},3000)



	var frameProportion = 1.78,
		frames = 25,
		resize = false;

	setLayerDimensions();
	$(window).on('resize', function(){
		if( !resize ) {

			resize = true;
			(!window.requestAnimationFrame) ? setTimeout(setLayerDimensions, 300) : window.requestAnimationFrame(setLayerDimensions);
		}
	});





	function setLayerDimensions() {
		var windowWidth = $(window).width(),
			windowHeight = $(window).height(),
			layerHeight, layerWidth;

		if( windowWidth/windowHeight > frameProportion ) {
			layerWidth = windowWidth;
			layerHeight = layerWidth/frameProportion;
		} else {
			layerHeight = windowHeight*1.2;
			layerWidth = layerHeight*frameProportion;
		}

		transitionBackground.css({
			'width': layerWidth*frames+'px',
			'height': layerHeight+'px',
		});

		resize = false;
	}



});

// CHECK ON MCHIMP
$('#mc-embedded-subscribe').click(function() {
    if ($('#acceptterms').prop('checked') == false) {
       $('#checkbox_mandatory, #checkbox_mandatory a').css('color','#BD0700')
        return false;
    }
});


$(window).scroll(function() {
    var scrollTop = $(this).scrollTop();
    var winheight=$(window).height()
    var converted= convertRange( scrollTop, [ 0, winheight ], [ 1, 500 ] );
    var convertedOpacity= convertRange( scrollTop, [ 0, winheight ], [ 1, 0 ] );
    console.log(converted)
    $('.realtime_text').css({
        left: converted+'px',
        opacity:convertedOpacity+0.1
    });

    $('.bakeoff_modal').css({
        right: -converted+40+'px',
        opacity:convertedOpacity+0.1
    });

    $('.scroll_down').css({
        opacity:convertedOpacity-0.5
    });
});
function convertRange( value, r1, r2 ) {
    return ( value - r1[ 0 ] ) * ( r2[ 1 ] - r2[ 0 ] ) / ( r1[ 1 ] - r1[ 0 ] ) + r2[ 0 ];
}
